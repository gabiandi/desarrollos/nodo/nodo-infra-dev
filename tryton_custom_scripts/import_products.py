#!/usr/bin/env python3

"""Script de importación de datos."""
import base64
import csv
import sys
from decimal import Decimal
from pathlib import Path
from csv import DictReader
from io import StringIO
from argparse import ArgumentParser
import argcomplete
from proteus import config, Model
from .utils import progress


def do_import():
    """Función de importación de datos."""
    print('Importando productos:')

    # Módelos de Tryton
    ProductTemplate = Model.get('product.template')
    Product = Model.get('product.product')
    ProductImage = Model.get('product.image')

    # Archivo de datos
    data_path = str(Path(__file__).parent / 'products.csv')

    csv.field_size_limit(sys.maxsize)

    with open(data_path, 'r', encoding='utf-8') as file:
        csv_data = file.read()

    data = DictReader(StringIO(csv_data), delimiter=';')

    # Recorrido de las filas de los datos
    for row in progress(list(data)):
        product_template, = ProductTemplate.find([('code', '=', row['template'])])

        try:
            product, = Product.find([
                ('code', '=', f'{row["template"]}{row["suffix_code"]}'),
            ])
        except ValueError:
            product = Product()
            product.suffix_code = row['suffix_code']

        product.template = product_template
        product.description = row['description']
        product.cost_price = Decimal(row['cost_price'])

        ProductImage.delete(product.images)
        product.images.clear()
        product.save()

        images = []
        for image_base64 in row['images'].split(','):
            product_image = product.images.new()
            product_image.image = base64.decodebytes(bytes(image_base64, 'utf-8'))
            product_image.product = product
            product_image.template = product_template
            images.append(product_image)
        ProductImage.save(images)


def main():
    """Función principal."""
    parser = ArgumentParser()
    parser.add_argument('-d', '--database', dest='database', required=True)
    parser.add_argument('-u', '--user', dest='user', default='admin')
    parser.add_argument('-c', '--config', dest='config_file', default=None)
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    config.set_trytond(args.database, args.user, config_file=args.config_file)
    with config.get_config().set_context(active_test=False):
        do_import()


if __name__ == '__main__':
    main()
