#!/usr/bin/env python3

"""Script de importación de datos."""
import base64
import csv
import sys
from pathlib import Path
from csv import DictReader
from io import StringIO
from argparse import ArgumentParser
import argcomplete
from proteus import config, Model
from .utils import progress


def do_import():
    """Función de importación de datos."""
    print('Importando noticias:')

    # Módelos de Tryton
    News = Model.get('nodo.news')

    # Archivo de datos
    data_path = str(Path(__file__).parent / 'news.csv')

    csv.field_size_limit(sys.maxsize)

    with open(data_path, 'r', encoding='utf-8') as file:
        csv_data = file.read()

    data = DictReader(StringIO(csv_data), delimiter=';')

    # Recorrido de las filas de los datos
    for row in progress(list(data)):
        try:
            new, = News.find([('title', '=', row['title'])])
        except ValueError:
            new = News(title=row['title'])

        new.title = row['title']
        new.url = row['url']
        new.image = base64.decodebytes(bytes(row['image'], 'utf-8'))
        new.publish_date = row['publish_date']
        new.save()


def main():
    """Función principal."""
    parser = ArgumentParser()
    parser.add_argument('-d', '--database', dest='database', required=True)
    parser.add_argument('-u', '--user', dest='user', default='admin')
    parser.add_argument('-c', '--config', dest='config_file', default=None)
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    config.set_trytond(args.database, args.user, config_file=args.config_file)
    with config.get_config().set_context(active_test=False):
        do_import()


if __name__ == '__main__':
    main()
