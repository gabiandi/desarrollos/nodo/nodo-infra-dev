"""Utilidades comunes a los scripts."""
from progressbar import ETA, Bar, ProgressBar, SimpleProgress


def progress(iterable):
    """Barra de progreso."""
    pbar = ProgressBar(widgets=[SimpleProgress(), Bar(), ETA()])
    return pbar(iterable)
