#!/usr/bin/env python3

"""Script de exportación de datos."""
from pathlib import Path
from csv import DictWriter
from argparse import ArgumentParser
import argcomplete
from proteus import Model, config
from .utils import progress


def do_export(limit: int = None):
    """Función de exportación de datos."""
    print('Exportando productos:')

    # Módelos de Tryton
    ProductTemplate = Model.get('product.template')
    records = ProductTemplate.find(limit=limit)

    # Archivo de datos
    data_path = str(Path(__file__).parent / 'product_templates.csv')

    # Recorrido de las filas de los datos
    fieldnames = {
        'name',
        'code',
        'type',
        'consumable',
        'salable',
        'purchasable',
        'default_uom',
        'categories',
        'list_price',
        'cost_price_method',
        'sale_uom',
    }

    with open(data_path, 'w', encoding='utf-8') as csv_file:
        writer = DictWriter(csv_file, fieldnames=fieldnames, delimiter=';')
        writer.writeheader()

        for record in progress(records):
            writer.writerow({
                'name': record.name,
                'code': record.code,
                'type': record.type,
                'consumable': '1' if record.consumable else '0',
                'salable': '1' if record.salable else '0',
                'purchasable': '1' if record.salable else '0',
                'default_uom': record.default_uom.symbol,
                'categories': ','.join([category.name for category in record.categories]),
                'list_price': record.list_price,
                'cost_price_method': record.cost_price_method,
                'sale_uom': record.sale_uom.symbol,
            })


def main():
    """Función principal."""
    parser = ArgumentParser()
    parser.add_argument('-d', '--database', dest='database', required=True)
    parser.add_argument('-u', '--user', dest='user', default='admin')
    parser.add_argument('-c', '--config', dest='config_file', default=None)
    parser.add_argument('-l', '--limit', dest='limit', type=int, default=None)
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    config.set_trytond(args.database, args.user, config_file=args.config_file)
    with config.get_config().set_context(active_test=False):
        do_export(args.limit)


if __name__ == '__main__':
    main()
