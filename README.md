# Preparación de entorno de desarrollo

## Preconfiguración con datos

Si usted esta probando en una infra local, puede popular los datos de prueba ejecutando:

~~~bash
source ../nodo-infra/.env
docker compose -f ../nodo-infra/docker-compose.yml run -u root -v ${PWD}/.:/tmp/nodo -e DB_NAME=${DATABASE_DB} --rm tryton /tmp/nodo/tryton_setup_scenario
~~~

## Inicio en modo dev

El modo dev implica montar las carpetas de los archivos de los proyectos dentro de los contenedores para tener reload automatico con cada modificación, sin la necesidad de recontruir las imagenes.

Para poder realizar esto, lo que se debe es de modificar el archivo *docker-compose-dev.yml* para apuntar a las rutas del proyecto, en el archivo de ejemplo estan las rutas como si el proyecto de la infra estubiera en la carpeta padre.

Luego puede levantar el entorno ejecutando:

~~~bash
docker compose -f ../nodo-infra/docker-compose.yml -f docker-compose-dev.yml up -d
~~~

## Comandos al entorno

Para ejecutar los comandos a la infraestructura siempre debe de referencias a los archivos:

~~~bash
docker compose -f ../nodo-infra/docker-compose.yml -f docker-compose-dev.yml {comando}
~~~
