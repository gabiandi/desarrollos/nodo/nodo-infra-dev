#!/usr/bin/env python3

"""Script de exportación de datos."""
import base64
from pathlib import Path
from csv import DictWriter
from argparse import ArgumentParser
import argcomplete
from proteus import Model, config
from .utils import progress


def do_export(limit: int = None):
    """Función de exportación de datos."""
    print('Exportando noticias:')

    # Módelos de Tryton
    News = Model.get('nodo.news')
    records = News.find(limit=limit)

    # Archivo de datos
    data_path = str(Path(__file__).parent / 'news.csv')

    # Recorrido de las filas de los datos
    fieldnames = {
        'title',
        'url',
        'image',
        'publish_date',
    }

    with open(data_path, 'w', encoding='utf-8') as csv_file:
        writer = DictWriter(csv_file, fieldnames=fieldnames, delimiter=';')
        writer.writeheader()

        for record in progress(records):
            writer.writerow({
                'title': record.title,
                'url': record.url,
                'image': base64.b64encode(record.image).decode('utf-8'),
                'publish_date': record.publish_date,
            })


def main():
    """Función principal."""
    parser = ArgumentParser()
    parser.add_argument('-d', '--database', dest='database', required=True)
    parser.add_argument('-u', '--user', dest='user', default='admin')
    parser.add_argument('-c', '--config', dest='config_file', default=None)
    parser.add_argument('-l', '--limit', dest='limit', type=int, default=None)
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    config.set_trytond(args.database, args.user, config_file=args.config_file)
    with config.get_config().set_context(active_test=False):
        do_export(args.limit)


if __name__ == '__main__':
    main()
