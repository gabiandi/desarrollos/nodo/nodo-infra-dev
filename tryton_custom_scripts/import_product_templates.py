#!/usr/bin/env python3

"""Script de importación de datos."""
import csv
import sys
from pathlib import Path
from csv import DictReader
from io import StringIO
from argparse import ArgumentParser
import argcomplete
from proteus import config, Model, Decimal
from .utils import progress


def do_import():
    """Función de importación de datos."""
    print('Importando plantillas de productos:')

    # Módelos de Tryton
    ProductTemplate = Model.get('product.template')
    ProductUom = Model.get('product.uom')
    ProductCategory = Model.get('product.category')

    # Archivo de datos
    data_path = str(Path(__file__).parent / 'product_templates.csv')

    csv.field_size_limit(sys.maxsize)

    with open(data_path, 'r', encoding='utf-8') as file:
        csv_data = file.read()

    data = DictReader(StringIO(csv_data), delimiter=';')

    # Recorrido de las filas de los datos
    for row in progress(list(data)):
        try:
            product_template, = ProductTemplate.find([('code', '=', row['code'])])
        except ValueError:
            product_template = ProductTemplate(code=row['code'])

        product_template.products.clear()
        product_template.name = row['name']
        product_template.type = row['type']
        product_template.consumable = row['consumable'] != '0'
        product_template.salable = row['salable'] != '0'
        product_template.purchasable = row['purchasable'] != '0'
        product_template.default_uom, = ProductUom.find(['symbol', '=', row['default_uom']])
        categories = ProductCategory.find(['name', 'in', row['categories'].split(',')])
        product_template.categories.extend(categories)
        product_template.list_price = Decimal(row['list_price'])
        product_template.cost_price_method = row['cost_price_method']
        product_template.sale_uom, = ProductUom.find(['symbol', '=', row['sale_uom']])
        product_template.save()


def main():
    """Función principal."""
    parser = ArgumentParser()
    parser.add_argument('-d', '--database', dest='database', required=True)
    parser.add_argument('-u', '--user', dest='user', default='admin')
    parser.add_argument('-c', '--config', dest='config_file', default=None)
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    config.set_trytond(args.database, args.user, config_file=args.config_file)
    with config.get_config().set_context(active_test=False):
        do_import()


if __name__ == '__main__':
    main()
